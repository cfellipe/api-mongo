let express = require('express');
let bodyParser = require('body-parser');
const mongoose = require('mongoose');
let routerProduct = require('./routes/product.router');
let app = express();



let dev_db_url = 'mongodb://admin:123456admin@ds137340.mlab.com:37340/mongo_project';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/product',routerProduct);


app.listen(3000, () => {
    console.log('App de Exemplo escutando na porta 3000');
  });