const Product = require('../models/product.model');

class ProductService {

    async insert(body) {

        let produto = new Product({
            name: body.nome,
            price: body.preco
        })

        return await Product.create(produto);
    }

    getById(id) {
        return new Promise((resolve, reject) => {
            Product.findById(id, (err, item) => {
                if (err) {
                    reject();
                }
                resolve(item);
            })
        })
    }

    update(id, body) {
        return new Promisse((resolve, reject) => {
            Product.findByIdAndUpdate(req.params.id, { $set: body }, (err, item) => {
                if (err) {
                    reject();
                }
                resolve(item)
            });
        })

    }
    remove(id) {
        return new Promisse((resolve, reject) => {
            Product.remove({ _id: id }, (err, item) => {
                if (err) {
                    reject();
                }
                resolve(item);
            })
        })
    }
}

module.exports = new ProductService();