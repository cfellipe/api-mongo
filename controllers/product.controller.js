
const ProductService = require('../services/product.service');

class ProductController {
    create (req, res){
        ProductService.insert(req.body)
            .then(produto => {
                res.send(produto);
            })

    }

    getById(req, res){
        ProductService.getById(req.params.id)
            .then(produto => {
                res.send(produto);
            })
    };

    update(req, res){
        ProductService.update(req.params.id, req.body)
            .then(produto => {
                res.send(produto);
            })
    };

    remove(req, res){
        ProductService.remove(req.params.id)
            .then(produto => {
                res.send(produto);
            })
    };
}

module.exports = new ProductController();