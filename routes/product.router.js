const express = require('express');
const router = express.Router();
const product_controller = require('../controllers/product.controller');

module.exports = router.get('/:id', product_controller.getById)
.post('/',product_controller.create)
.delete('/:id',product_controller.remove)
.put('/:id', product_controller.update);